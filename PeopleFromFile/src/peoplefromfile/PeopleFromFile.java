package peoplefromfile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

class Person {

    Person(String n, int a) {
        name = n;
        age = a;
    }
    String name;
    int age;
}

public class PeopleFromFile {

    final static String FILE_NAME = "people.txt";

    static ArrayList<Person> people = new ArrayList<>();

    public static void main(String[] args) {
        try {
            Scanner fileInput = new Scanner(new File(FILE_NAME));
            while (fileInput.hasNextLine()) {
                String name = fileInput.nextLine();
                int age = fileInput.nextInt();
                fileInput.nextLine();
                //
                Person p = new Person(name, age);
                people.add(p);
            }
            fileInput.close();
            //
            for (Person p : people) {
                System.out.printf("p: name=%s, age=%d\n",
                        p.name, p.age);
            }

        } catch (IOException e) {
            System.err.println("Error reading file");
        } catch (InputMismatchException e) {
            System.err.println("Error: file contents mismatch");
        }
    }

}