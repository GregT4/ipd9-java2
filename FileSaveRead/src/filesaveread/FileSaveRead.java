
package filesaveread;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
public class FileSaveRead {

    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        
                      
        File myFile = new File("data.txt");
        
        String myname; 
        
        try {
            
            PrintWriter fileoutput = new PrintWriter(myFile);
            
                System.out.println("Enter a name: ");
                myname = input.nextLine();
                fileoutput.println("" + myname);
                                       
            fileoutput.close();
        }
            catch (Exception e){
                  e.printStackTrace();
            }
          
        
        try {
            
            Scanner fileInput = new Scanner(myFile);
            
                        
            while (fileInput.hasNext()){
                myname = fileInput.nextLine();
                System.out.println("Read line:" + myname);
            }
                        
            fileInput.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        
    }
}
