
package quiz2passports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

class Passport {
    
    public Passport(String number, String firstName, String lastName, String address, String city, double heightCm,
           double weightKg, int yob){
	setNumber(number);
        setFirstName(firstName);
        setLastName(lastName);
        setAddress(address);
        setCity(city);
        setHeightCm(heightCm);
        setWeightKg(weightKg);
        setYob(yob);
    }
    private String number; 
    private String firstName; 
    private String lastName;
    private String address;
    private String city;
    private double heightCm;
    private double weightKg;
    private int yob;

    
    public String getNumber() {
        return number;
    }

    
    public void setNumber(String number) {
        if (!number.matches("[A-Z]{2} + (0-9){6}")) {
            throw new IllegalArgumentException("Passprt number must be 2 uppercase letters and 6 numbers" + number);
        } 
        this.number = number;
    }

    
    public String getFirstName() {
        return firstName;
    }

    
    public void setFirstName(String firstName) {
        if (!firstName.matches("[A-Z]>={2}")){
            throw new IllegalArgumentException("First name must be min 2 letters" + firstName);
        } 
        this.firstName = firstName;
    }

    
    public String getLastName() {
        return lastName;
    }

    
    public void setLastName(String lastName) {
        if (!lastName.matches("[A-Z]>={2}")){
            throw new IllegalArgumentException("Last name must be min 2 letters" + lastName);
        } 
        this.lastName = lastName;
    }

    
    public String getAddress() {
        return address;
    }

    
    public void setAddress(String address) {
        if (!address.matches("[A-Z]>={2}")){
            throw new IllegalArgumentException("Address must be min 2 letters" + address);
        } 
        this.address = address;
    }

    
    public String getCity() {
        return city;
    }

    
    public void setCity(String city) {
        if (!lastName.matches("[A-Z]>={2}")){
            throw new IllegalArgumentException("City must be min 2 letters" + lastName);
        } 
        this.city = city;
    }

    
    public double getHeightCm() {
        return heightCm;
    }

    
    public void setHeightCm(double heightCm) {
        if ( heightCm < 0 && heightCm > 300){
            throw new IllegalArgumentException("Heightkg must be between from 0-300");
        }
        this.heightCm = heightCm;
    }

    
    public double getWeightKg() {
        return weightKg;
    }

    
    public void setWeightKg(double weightKg) {
        if ( weightKg < 0 && weightKg > 300){
            throw new IllegalArgumentException("Weightkg must be between from 0-300");
        }
        this.weightKg = weightKg;
    }

    
    public int getYob() {
        return yob;
    }

    
    public void setYob(int yob) {
        if ( yob < 1900 && yob > 2050){
            throw new IllegalArgumentException("Year of birth must be between 1900-2050");
        }
        this.yob = yob;
    }

    
}

public class Quiz2Passports {

    static final String FILE_PASSPORT = "passports.txt";   
    
    static ArrayList<Passport> passportList = new ArrayList<>();
    
    static void readPassport() throws FileNotFoundException {
        Scanner fileInput = new Scanner(new File(FILE_PASSPORT));
        while (fileInput.hasNextLine()) {
            String fileLine = fileInput.nextLine();
            String data[] = fileLine.split(";");
            if (data.length != 8) {
                throw new IllegalArgumentException("Line malformed: " + fileLine);
            }
            String number = data[0];
            String firstName = data[1];
            String lastName = data[2];
            String address = data[3];
            String city = data[4];
            double heightCm = Double.parseDouble(data[5]);
            double weightKg = Double.parseDouble(data[6]);
            int yob = Integer.parseInt(data[7]);
            passportList.add(new Passport( number, firstName, lastName, address, city, heightCm, weightKg, yob));
        }
        fileInput.close();
    }
    static Scanner input = new Scanner(System.in);
    
    private static int getMenuChoice() {
        while (true) {
            System.out.println("1. Display all passports data\n"
                    + "2. Display all passports for people in the same city\n"
                    + "3. Find the tallest person and display their info\n"
                    + "4. Find the lightest person and display their info\n"
                    + "5. Display all people younger than certain age\n"
                    + "6. Add person to list\n"
                    + "0. Save data back to file and exit.");
            System.out.print("Enter choice [0-6]: ");
            try {
                int choice = input.nextInt();
                input.nextLine();
                if (choice < 0 || choice > 4) {
                    System.out.println("Invalid choice, try again");
                } else {
                    return choice;
                }
            } catch (InputMismatchException e) {
                input.nextLine();
                System.out.println("Invalid choice, try again");
            }
        }
    }
        
    
    public static void main(String[] args) {
        
        try {
            readPassport();
            while (true) {
                int choice = getMenuChoice();
                switch (choice) {
                    case 0:
                        saveAllPassports();
                        System.out.println("Bye bye");                        
                        return;
                    case 1:
                       ListAllPassports();
                        break;    
                    case 2:
                        System.out.print("Enter city name: ");
                        findPassportInCity();
                        break;
                    case 3:
                        fingTallestPerson();
                        break;
                    case 4:
                        findLightestPerson();
                        break;
                    case 5:
                        findYoungerThan();
                        break;
                    case 6:
                        addPassport();
                        break;
                    default:
                        System.out.println("Internal fatal error. Terminating.");
                        System.exit(1);
                }
            }
        } catch (IOException e) {
            System.out.println("Error reading file: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println("Error reading file: " + e.getMessage());
        }
    }

    private static void saveAllPassports() {
         try {
            PrintWriter pw = new PrintWriter(new FileWriter(FILE_PASSPORT));
            for (Passport a : passportList) {
                pw.printf("%s;%s;%s;%s;%s;%d;%d;\n", a.getNumber(), a.getFirstName(),
                        a.getLastName(), a.getAddress(), a.getCity(), a.getHeightCm(), a.getWeightKg(), a.getYob() );
            }
            pw.close();
        } catch (IOException e) {
            System.err.println("Error saving to file");
            // FIXME close the file anyway
        }
    }

    private static void ListAllPassports() {
        
        for (int i = 0; i<passportList.size(); i++) {
            Passport a = passportList.get(i);
            System.out.printf("%s;%s;%s;%s;%s;%d;%d;\n", a.getNumber(), a.getFirstName(),
                        a.getLastName(), a.getAddress(), a.getCity(), a.getHeightCm(), a.getWeightKg(), a.getYob() );
        }       
    }

    private static void findPassportInCity() {
     
        for (Passport a: passportList) {
            if (a.getCity().equals(city)) {
                return a;
            }
             return null;
        }
    }    

    private static void fingTallestPerson() {
        
        Passport a = passportList.get(0);
        
        for (HPassport a: passportList) {
            if (a.getHeightCm()<(HeightCm)) {
                return a;
            }
             return null;
        }
    }
}
