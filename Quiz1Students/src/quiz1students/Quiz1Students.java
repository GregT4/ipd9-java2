
package quiz1students;

import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Quiz1Students {

    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.println("1. Add student and their GPA\n"
                + "2. List all students and their GPAs\n"
                + "3. Find all students whose name begins with a letter\n"
                + "4. Find the average GPA of all students and display it.\n"
                + "0. Exit");
        int choice = inputInt("Choice: ");
        
        if ((choice < 0) || (choice > 4)) {
            System.out.println("Invalid choice, please try again.");
            choice = inputInt("Choice: ");
            return;
        }
        switch (choice) {
            case 0:
                System.out.println("Bye bye");
                return;
            case 1:
                addStudentandGpa();
                break;
            case 2:
                listStudentandGpa();
                break;
            case 3:
                findStudentFirstLetter();
                break;
            case 4:
                findAvgGpaDisplay();
                break;
            default:
                System.out.println("Fatal Error");
                System.exit(1);
        }
    }

    static int inputInt(String message) {
        for (;;) {
            try {
                System.out.print(message);
                int value = input.nextInt();
                input.nextLine(); 
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); 
                System.out.println("Invalid input, try again");
            }
        }
    }
    
    private static final String FILE_NAME = "students.txt";
    
    private static void addStudentandGpa() {
        System.out.println("Adding a student.");
        System.out.print("Enter student's name:  ");
        String name = input.nextLine();
        System.out.print("Enter student's GPA:  ");
        double gpa = input.nextDouble(); 
        if ((gpa < 0)||(gpa>4.3)){
            System.out.println("Invalid entry");
            return;
        }
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(FILE_NAME, true));
            pw.printf("%s\n%.2f\n", name, gpa);
            pw.close();
            
            System.out.println("Student added.");
         
        } catch (IOException ioe) {
            System.out.println("Error writing to file. Person not added.");
        }
    }

    private static void listStudentandGpa() {
        
        System.out.println("Listing all students.");
        
        try {
            File file = new File(FILE_NAME);
            if (file.exists()) {
                Scanner fileInput = new Scanner(file);
                while (fileInput.hasNextLine()) {
                    String name = fileInput.nextLine();
                    double gpa = fileInput.nextDouble(); 
                    fileInput.nextLine();
                    System.out.printf("%s\n %.2f\n", name, gpa);
                }
                fileInput.close();
            } else {
                System.out.println("File does not exist");
            }
        } catch (IOException ex) {
            System.out.println("Error reading file");
        }
    }

    private static void findStudentFirstLetter() {
        
        System.out.println("Enter first letter of student name (only one letter): ");
        
        try {
            File file = new File(FILE_NAME);
            if (file.exists()) {
                Scanner fileInput = new Scanner(file);
                byte search = input.nextByte();
                while (fileInput.hasNextLine()) {
                    String name = fileInput.nextLine();
                    int gpa = fileInput.nextInt();
                    fileInput.nextLine();
                    

                    if (search.contains()) { //FIXME: parameters
                        System.out.printf("%s is %.2f\n", name, gpa);
                    }
                }
                fileInput.close();
            } else {
                System.out.println("File does not exist");
            }
        } catch (IOException ex) {
            System.out.println("Error reading file");
        }
    }

    private static void findAvgGpaDisplay() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
