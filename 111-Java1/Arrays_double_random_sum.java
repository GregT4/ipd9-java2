
import java.util.Scanner;

public class Arrays_double_random_sum  {
   

    public static void main(String[] args) {
    

        Scanner input = new Scanner(System.in);

        int[][]myTable;
        

        myTable = new int[4][4];

        for (int row = 0; row < myTable.length; row++){
    
            for(int column = 0; column < myTable[row].length; column++){
               myTable[row][column]= (int)(Math.random()*10);
            }
        }
        
        for (int row = 0; row < myTable.length; row++){
            for (int column = 0; column < myTable[row].length; column++){
                System.out.print(myTable[row][column] + " ");
            }
            System.out.println();
        }
        
        for (int column = 0; column < myTable[0].length; column++){
            
            int total = 0;
            
            for (int row = 0; row < myTable.length; row++){
                total += myTable[row][column];
            }
            System.out.println("Sum for column " + column + " is " + total);
        }
 
  }
}