import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class Read_IO_01{

    //Read a file//
    
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        File myFile = new File("mytextfile.txt");
        
        
        
        String myname; 
        
        try {
            
            Scanner fileInput = new Scanner(myFile);
            
                        
            while (fileInput.hasNext()){
                myname = fileInput.next();
                System.out.println(myname);
            }
                        
            fileInput.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
