/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class MoocExcerises 
{    
    public static void main (String[] args)
    {
        System.out.println("Gregory Torakis");
        
        String text = "includes text";
        int wholeNumber = 123;
        double decimalNumber = 3.141592653;
        boolean isTrue = true;
        System.out.println("The variable's type is text. Its value is " + text);
        System.out.println("The variable's type is integer. Its value is " + wholeNumber);
        System.out.println("The variable's type is decimal number. Its value is  " + decimalNumber);
        System.out.println("The variable's type is truth. Its value is  " + isTrue);
        
        wholeNumber = 42;
        System.out.println("The variable's type is integer. Its value is " + wholeNumber);
        
        int first = 1;
        int second = 2;
        int sum = first + second;
        System.out.println(sum);
        System.out.println(first + second);
        System.out.println(2 + second - first - second);
        
        int remainder = 7 % 2; //remainder is 1 (integer)//
        System.out.println("The remainder of 7 % 2 is " + remainder);
        
        double whenDivisionIsFloat = 3.0 / 2;
        System.out.println(whenDivisionIsFloat);
        
        int dividend = 3;
        int divisor = 2;
        
        double qoutient = 1.0 * dividend / divisor;
        System.out.println(qoutient);
        
        int seconds = 60;
        int minutes = 60;
        int hours = 24;
        int days = 365;
        
        int year = seconds * minutes * hours * days;
        System.out.print("There are "+ year +" seconds in a year");
    }
}
