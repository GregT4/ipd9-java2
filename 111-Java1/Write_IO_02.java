
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class Write_IO_02 {

    //Write to a file//
    
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        File myFile = new File("mytextfile.txt");
        
        String myname; 
        
        try {
            
            PrintWriter fileoutput = new PrintWriter(myFile);
            
            int count = 0;
            
            while (count <10){
                System.out.println("Enter a name: ");
                myname = input.next();
                fileoutput.println("" + myname);
                count++;
                
                //SAME AS ABOVE//
                
//               for (int i = 0; i < 10;i++){
//                    System.out.println("Enter a name: ");
//                    myname = input.next();
//                    fileoutput.println("" + myname);
//               }
               
            }
                        
            fileoutput.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
