
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class Write_IO_01 {

    //Write to a file//
    
    public static void main(String[] args) {
        
        File myFile = new File("mytextfile.txt");
        
        try {
            
            PrintWriter fileoutput = new PrintWriter(myFile);
            
            fileoutput.println("Gregory Torakis");
            fileoutput.println("Jose Porras");
            
            fileoutput.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
}
