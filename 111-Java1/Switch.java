/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Scanner;
/**
 *
 * @author student
 */
public class Switch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {    
        Scanner input = new Scanner(System.in);
        
        int number = 0;
        
        System.out.println("Please enter a number");
        number = input.nextInt();
        
        switch (number%3) 
        {   
            case 0: 
                System.out.println("Number " + number + " is divisible by 3");
                break;
            case 1:
                System.out.println("Number " + number + " is not divisible by 3");
                System.out.println("The remainder of the number is: 1");
                break;                
            case 2:
                System.out.println("Number " + number + " is not divisible by 3"); 
                System.out.println("This number has a remainder of 2");
                break;
        }
    }
    
}
