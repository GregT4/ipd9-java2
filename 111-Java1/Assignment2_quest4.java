
import java.util.Scanner;

public class Assignment2_quest4 {

    
    public static void main(String[] args) 
    {    
        Scanner input = new Scanner(System.in);
        
        int year = 0;
        
        System.out.println("Please enter year");
        year = input.nextInt();
        
        switch (year%12) 
        {   
            case 0: 
                System.out.println("You're Chinese sign is a monkey");
                break;
            case 1:
                System.out.println("You're Chinese sign is a rooster");
                break;                
            case 2:
                System.out.println("You're Chinese sign is a dog"); 
                break;
            case 3:
                System.out.println("You're Chinese sign is a pig"); 
                break;
            case 4:
                System.out.println("You're Chinese sign is a rat"); 
                break;
            case 5:
                System.out.println("You're Chinese sign is a ox"); 
                break;
            case 6:
                System.out.println("You're Chinese sign is a tiger"); 
                break;
            case 7:
                System.out.println("You're Chinese sign is a rabbit"); 
                break;  
            case 8:
                System.out.println("You're Chinese sign is a dragon"); 
                break;
            case 9:
                System.out.println("You're Chinese sign is a snake"); 
                break;
            case 10:
                System.out.println("You're Chinese sign is a horse"); 
                break;
            case 11:
                System.out.println("You're Chinese sign is a sheep"); 
                break; 
            default:
                System.out.println("Choose a number from 0-12"); 
                break;    
        }
    }
    
}
