/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Scanner;

public class SomeProducts {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
     int nom1 = 0;
     int nom2 = 0;
     int theSum = 0;
     int theProduct = 0;
             
     Scanner input = new Scanner(System.in);
     
     System.out.print("Please enter first number");
     nom1 = input.nextInt();
     System.out.print("Please enter second number");
     nom2 = input.nextInt();
     
     theSum = nom1 + nom2;
     theProduct = nom1 * nom2;
     
     System.out.println("The sum of the numbers is:" + theSum);
     System.out.println("The product of the numbers is:" + theProduct);
     
    }
    
}
